#include "Landscape.h"

Landscape::Landscape(unsigned int l, float c, float s)
	: data(NULL), indeces(NULL), indexTop(0), sizeLog(l), bottomLim(0.0f),
	topLim(3.0f), edgeCoef(c), scale(s), yMin(0.0f), yMax(3.0f)
{
	size = (1 << sizeLog) + 1;
}

Landscape::~Landscape()
{
	glDeleteBuffers(1, &vertexDataBufferHandle);
	glDeleteBuffers(1, &vertexIndexBufferHandle);
	glDeleteVertexArrays(1, &vertexArrayHandle);
	if (data)
		delete[] data;
}

void Landscape::generateData()
{
	if (data)
		delete[] data;
	if (indeces)
		delete[] indeces;

	//data = new VertexData[size * size * 2];
	data = new VertexData[size * size];
	//indeces = new unsigned int[3 * 2 * (size - 1) * (size - 1) + 3 * size * size];
	indeces = new unsigned int[3 * 2 * (size - 1) * (size - 1)];
	srand(time(NULL));
	hight(0, 0) = random1();
	hight(0, size - 1) = random1();
	hight(size - 1, 0) = random1();
	hight(size - 1, size - 1) = random1();
	recurseSetMap(0, 0, size - 1, RSM_ALL);
	/*
	 *for (unsigned int i = 0; i < 3; ++i)
	 *    for (unsigned int j = 0; j < 3; ++j) {
	 *        cout << "pos(" << i << ',' << j << ") = {" << data[size * i + j].pos.x << ',' << data[size * i + j].pos.y << ',' << data[size * i + j].pos.z << "}\n";
	 *        cout << "norm(" << i << ',' << j << ") = {" << data[size * i + j].nor.x << ',' << data[size * i + j].nor.y << ',' << data[size * i + j].nor.z << "}\n";
	 *    }
	 */
	for (unsigned int row = 0; row < size; ++row)
		for (unsigned int col = 0; col < size; ++col)
			setNorm(row, col);
	/*
	 *for (unsigned int i = 0; i < size - 1; ++i) {
	 *    for (unsigned int j = 0; j < size; ++j) {
	 *        data[i * size + j + size * size].pos = data[i * size + j].nor + data[i * size + j].pos;
	 *        data[i * size + j + size * size].nor = data[i * size + j].nor;
	 *        data[i * size + j + size * size].tex = data[i * size + j].tex;
	 *        indeces[indexTop++] = i * size + j + size * size;
	 *        indeces[indexTop++] = i * size + j;
	 *        indeces[indexTop++] = (i + 1) * size + j;
	 *    }
	 *}
	 *for (unsigned int j = 0; j < size - 1; ++j) {
	 *    data[(size - 1) * size + j + size * size].pos = data[(size - 1) * size + j].nor + data[(size - 1) * size + j].pos;
	 *    data[(size - 1) * size + j + size * size].nor = data[(size - 1) * size + j].nor;
	 *    data[(size - 1) * size + j + size * size].tex = data[(size - 1) * size + j].tex;
	 *    indeces[indexTop++] = (size - 1) * size + j + size * size;
	 *    indeces[indexTop++] = (size - 1) * size + j;
	 *    indeces[indexTop++] = (size - 1) * size + j + 1;
	 *}
	 *data[(size - 1) * size + size - 1 + size * size].pos = data[(size - 1) * size + size - 1].nor + data[(size - 1) * size + size - 1].pos;
	 *data[(size - 1) * size + size - 1 + size * size].nor = data[(size - 1) * size + size - 1].nor;
	 *data[(size - 1) * size + size - 1 + size * size].tex = data[(size - 1) * size + size - 1].tex;
	 *indeces[indexTop++] = (size - 1) * size + size - 1 + size * size;
	 *indeces[indexTop++] = (size - 1) * size + size - 1;
	 *indeces[indexTop++] = (size - 1) * size + size - 1;
	 */
}

void Landscape::recurseSetMap(unsigned int rowOfs, unsigned int colOfs, unsigned int size, unsigned int type)
{
	if (type & RSM_LEFT)
		hight(rowOfs + size / 2, colOfs) = (pos(rowOfs, colOfs).y + pos(rowOfs + size, colOfs).y) / 2 + random1() * scale * size * edgeCoef;
	if (type & RSM_RIGHT)
		hight(rowOfs + size / 2, colOfs + size) = (pos(rowOfs, colOfs + size).y + pos(rowOfs + size, colOfs + size).y) / 2 + random1() * scale * size * edgeCoef;
	if (type & RSM_UP)
		hight(rowOfs, colOfs + size / 2) = (pos(rowOfs, colOfs).y + pos(rowOfs, colOfs + size).y) / 2 + random1() * scale * size * edgeCoef;
	if (type & RSM_DOWN)
		hight(rowOfs + size, colOfs + size / 2) = (pos(rowOfs + size, colOfs).y + pos(rowOfs + size, colOfs + size).y) / 2 + random1() * scale * size * edgeCoef;
	float avg = (pos(rowOfs + size / 2, colOfs).y
	           + pos(rowOfs + size / 2, colOfs + size).y
			   + pos(rowOfs, colOfs + size / 2).y
			   + pos(rowOfs + size, colOfs + size / 2).y) / 4;
	hight(rowOfs + size / 2, colOfs + size / 2) =  avg + random1() * scale * size * edgeCoef;

	if (size == 2) {
		endOfRecursion(rowOfs, colOfs, type);
	} else {
		size /= 2;
		recurseSetMap(rowOfs, colOfs, size, RSM_ALL);
		recurseSetMap(rowOfs + size, colOfs + size, size, RSM_ALL);
		recurseSetMap(rowOfs + size, colOfs, size, RSM_LEFT | RSM_DOWN);
		recurseSetMap(rowOfs, colOfs + size, size, RSM_RIGHT | RSM_UP);
	}
}

void Landscape::endOfRecursion(unsigned int rowOfs, unsigned int colOfs, unsigned int type)
{
	if (type & RSM_UP && type & RSM_LEFT) {
		setTextCoord(rowOfs + 0, colOfs + 0, 1.0f * (colOfs + 0) / (this->size - 1), 1.0f * (rowOfs + 0) / (this->size - 1));
		//setNorm(rowOfs + 0, colOfs + 0);
	} if (type & RSM_UP) {
		setTextCoord(rowOfs + 0, colOfs + 1, 1.0f * (colOfs + 1) / (this->size - 1), 1.0f * (rowOfs + 0) / (this->size - 1));
		//setNorm(rowOfs + 0, colOfs + 1);
	} if (type & RSM_UP && type & RSM_RIGHT) {
		setTextCoord(rowOfs + 0, colOfs + 2, 1.0f * (colOfs + 2) / (this->size - 1), 1.0f * (rowOfs + 0) / (this->size - 1));
		//setNorm(rowOfs + 0, colOfs + 2);
	} if (type & RSM_LEFT) {
		setTextCoord(rowOfs + 1, colOfs + 0, 1.0f * (colOfs + 0) / (this->size - 1), 1.0f * (rowOfs + 1) / (this->size - 1));
		//setNorm(rowOfs + 1, colOfs + 0);
	}
	setTextCoord(rowOfs + 1, colOfs + 1, 1.0f * (colOfs + 1) / (this->size - 1), 1.0f * (rowOfs + 1) / (this->size - 1));
	//setNorm(rowOfs + 1, colOfs + 1);
	if (type & RSM_RIGHT) {
		setTextCoord(rowOfs + 1, colOfs + 2, 1.0f * (colOfs + 2) / (this->size - 1), 1.0f * (rowOfs + 1) / (this->size - 1));
		//setNorm(rowOfs + 1, colOfs + 2);
	} if (type & RSM_DOWN && type & RSM_LEFT) {
		setTextCoord(rowOfs + 2, colOfs + 0, 1.0f * (colOfs + 0) / (this->size - 1), 1.0f * (rowOfs + 2) / (this->size - 1));
		//setNorm(rowOfs + 2, colOfs + 0);
	} if (type & RSM_DOWN) {
		setTextCoord(rowOfs + 2, colOfs + 1, 1.0f * (colOfs + 1) / (this->size - 1), 1.0f * (rowOfs + 2) / (this->size - 1));
		//setNorm(rowOfs + 2, colOfs + 1);
	} if (type & RSM_DOWN && type & RSM_RIGHT) {
		setTextCoord(rowOfs + 2, colOfs + 2, 1.0f * (colOfs + 2) / (this->size - 1), 1.0f * (rowOfs + 2) / (this->size - 1));
		//setNorm(rowOfs + 2, colOfs + 2);
	}
	// add vertex indeces into array
	pushPair(rowOfs, colOfs);
	pushPair(rowOfs, colOfs + 1);
	pushPair(rowOfs + 1, colOfs);
	pushPair(rowOfs + 1, colOfs + 1);
}

void Landscape::setNorm(unsigned int row, unsigned int col)
{
	if (pos(row, col).y < yMin) {
		yMin = pos(row, col).y;
		//cout <<	"yMin = " << (yMin = pos(row, col).y) << '\n';
	}
	if (pos(row, col).y > yMax) {
		yMax = pos(row, col).y;
		//cout << "yMax = " << (yMax = pos(row, col).y) << '\n';
	}
	glm::vec3 sum;
	sum += glm::cross(pos(row, col) - pos(row, col - 1), pos(row - 1, col - 1) - pos(row, col - 1)) * 1.6666f;
	sum += glm::cross(pos(row - 1, col - 1) - pos(row - 1, col), pos(row, col) - pos(row - 1, col)) * 1.6666f;
	sum += glm::cross(pos(row, col - 1) - pos(row, col), pos(row + 1, col) - pos(row, col)) * 1.6666f;
	sum += glm::cross(pos(row, col + 1) - pos(row, col), pos(row - 1, col) - pos(row, col)) * 1.6666f;
	sum += glm::cross(pos(row, col) - pos(row, col + 1), pos(row + 1, col + 1) - pos(row, col + 1)) * 1.6666f;
	sum += glm::cross(pos(row + 1, col + 1) - pos(row + 1, col), pos(row, col) - pos(row + 1, col)) * 1.6666f;
	if (sum.y < 0)
		cout << '(' << col << ',' << row << ")\n";
	data[row * size + col].nor = glm::normalize(sum);
}

void Landscape::initGLBuffers(GLuint programHandle, const char* posName, const char* norName, const char* texName)
{
	glGenVertexArrays(1, &vertexArrayHandle);
	glBindVertexArray(vertexArrayHandle);

	glGenBuffers(1, &vertexDataBufferHandle);
	glGenBuffers(1, &vertexIndexBufferHandle);
	
	glBindBuffer(GL_ARRAY_BUFFER, vertexDataBufferHandle);
	//glBufferData(GL_ARRAY_BUFFER, 2 * size * size * sizeof(VertexData), data, GL_STATIC_DRAW);
	glBufferData(GL_ARRAY_BUFFER, size * size * sizeof(VertexData), data, GL_STATIC_DRAW);
		
	glEnable(GL_ELEMENT_ARRAY_BUFFER);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexIndexBufferHandle);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexTop * sizeof(unsigned int), indeces, GL_STATIC_DRAW);
	
	int	loc;
	if ((loc = glGetAttribLocation(programHandle, posName)) != -1) {
		glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (GLuint *) 0);
		glEnableVertexAttribArray(loc);
	}
	if ((loc = glGetAttribLocation(programHandle, norName)) != -1) {
		glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (GLuint *) (0 + sizeof(float) * 3));
		glEnableVertexAttribArray(loc);
	}
	if ((loc = glGetAttribLocation(programHandle, texName)) != -1) {
		glVertexAttribPointer(loc, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (GLuint *) (0 + sizeof(float) * 6));
		glEnableVertexAttribArray(loc);
	}
	glBindVertexArray(0);
}

void Landscape::draw()
{
	glBindVertexArray(vertexArrayHandle);
	/*
	 *for (unsigned int i = 0; i < 3; ++i) {
	 *    for (unsigned int j = 0; j < 3; ++j) {
	 *        cout
	 *            << "pos[" << i << ',' << j << "] = ("
	 *            << data[i * size + j].pos.x << ","
	 *            << data[i * size + j].pos.y << ","
	 *            << data[i * size + j].pos.z << ") ";
	 *        cout
	 *            << "nor[" << i << ',' << j << "] = ("
	 *            << data[i * size + j].nor.x << ","
	 *            << data[i * size + j].nor.y << ","
	 *            << data[i * size + j].nor.z << ") ";
	 *        cout
	 *            << "tex[" << i << ',' << j << "] = ("
	 *            << data[i * size + j].tex.x << ","
	 *            << data[i * size + j].tex.y << ")\n";
	 *    }
	 *}
	 */
	glDrawElements(GL_TRIANGLES, indexTop, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}
