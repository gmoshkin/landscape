#pragma once

#include <glm/glm.hpp>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include "VertexData.h"

using namespace std;

class Landscape
{
	VertexData *data;
	unsigned int *indeces;
	unsigned int indexTop;

	unsigned char sizeLog;
	unsigned int size;
	float bottomLim;
	float topLim;
	float edgeCoef;
	float scale;
	float yMin;
	float yMax;

	unsigned int vertexArrayHandle;
	unsigned int vertexDataBufferHandle;
	unsigned int vertexIndexBufferHandle;

	enum RSMType
	{
		RSM_UP = 1,
		RSM_RIGHT = 2,
		RSM_DOWN = 4,
		RSM_LEFT = 8,
		RSM_ALL = 15
	};

	inline float& hight(unsigned int row, unsigned int col)
	{
		if (row < 0 || row >= size || col < 0 || col >= size) {
			throw "Index out of range";
		}
		data[row * size + col].pos.x = col * scale;
		data[row * size + col].pos.z = row * scale;
		return data[row * size + col].pos.y;
	}
	inline void setTextCoord(unsigned int row, unsigned int col, float x, float y)
	{
		if (row < 0 || row >= size || col < 0 || col >= size) {
			cout
				<< "row = " << row << " "
				<< "col = " << col << " "
				<< "size = " << size << " "
				<< "sizeLog = " << sizeLog << " "
				<< "indexTop = " << indexTop << " "
				<< "x = " << x << " "
				<< "y = " << y << "\n";
			throw "Index out of range";
		}
		data[row * size + col].tex.x = x;
		data[row * size + col].tex.y = y;
	}
	inline const glm::vec3 pos(int row, int col) const
	{
		if (row < 0 || row >= static_cast<long int>(size) || col < 0 || col >= static_cast<long int>(size))
			return glm::vec3(col * scale, 0.0f, row * scale);
		return data[row * size + col].pos;
	}
	inline float random() const
	{
		return static_cast<float>(rand()) / (topLim - bottomLim) + bottomLim;
	}
	inline float random1() const
	{
		return 2.0f * static_cast<float>(rand()) / RAND_MAX - 1.0f;
	}
	inline void pushPair(unsigned int row, unsigned int col)
	{
		indeces[indexTop++] = (row + 0) * size + (col + 0);
		indeces[indexTop++] = (row + 1) * size + (col + 0);
		indeces[indexTop++] = (row + 1) * size + (col + 1);
		indeces[indexTop++] = (row + 0) * size + (col + 0);
		indeces[indexTop++] = (row + 0) * size + (col + 1);
		indeces[indexTop++] = (row + 1) * size + (col + 1);
	}
	void setNorm(unsigned int row, unsigned int col);
	void recurseSetMap(unsigned int rowOfs, unsigned int colOfs, unsigned int size, unsigned int type);
	void endOfRecursion(unsigned int rowOfs, unsigned int colOfs, unsigned int type);

public:
	Landscape(unsigned int l = 3, float c = 0.3f, float s = 1.0f);
	~Landscape();

	inline void setSizeLog(unsigned char l)
	{
		size = (1 << (sizeLog = l)) + 1;
	}
	inline void setLowLim(float l)
	{
		bottomLim = l;
	}
	inline void setHighLim(float l)
	{
		topLim = l;
	}
	inline void setEdgeCoef(float c)
	{
		edgeCoef = c;
	}
	inline void setScale(float s)
	{
		scale = s;
	}
	inline float getYMin()
	{
		return yMin;
	}
	inline float getYMax()
	{
		return yMax;
	}
	void generateData();
	void initGLBuffers(GLuint programHandle, const char* posName, const char* norName, const char* texName);
	void draw();
};
