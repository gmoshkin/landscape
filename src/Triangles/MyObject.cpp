#include "MyObject.h"

using namespace glm;

MyObject::~MyObject()
{
    glDeleteBuffers(1, &vertexDataBufferHandle);
    glDeleteBuffers(1, &vertexIndexBufferHandle);
    glDeleteVertexArrays(1, &vertexArrayHandle);
    if (data)
        delete[] data;
}

void MyObject::generateData(vec3 eye, vec3 dir, vec3 up)
{
    if (data)
        delete[] data;
    if (indexes)
        delete[] indexes;

    data = new VertexData[4];
    indexes = new unsigned int[6];

    left = normalize(cross(up, dir));
    top = rotate(left, 3.14159265f, dir);
    cen = eye + dir;

    data[0].pos = vec3(cen + top + left);
    data[1].pos = vec3(cen + top - left);
    data[2].pos = vec3(cen - top - left);
    data[3].pos = vec3(cen - top + left);

    data[0].nor = -dir;
    data[1].nor = -dir;
    data[2].nor = -dir;
    data[3].nor = -dir;

    data[0].tex = vec2(0.0, 0.0);
    data[1].tex = vec2(0.0, 1.0);
    data[2].tex = vec2(1.0, 1.0);
    data[3].tex = vec2(1.0, 0.0);

    indexes[0] = 0;
    indexes[1] = 1;
    indexes[2] = 2;
    indexes[3] = 1;
    indexes[4] = 2;
    indexes[5] = 3;
}

void MyObject::initGLBuffers(GLuint programHandle, const char* posName, const char* norName, const char* texName)
{
    glGenVertexArrays(1, &vertexArrayHandle);
    glBindVertexArray(vertexArrayHandle);

    glGenBuffers(1, &vertexDataBufferHandle);
    glGenBuffers(1, &vertexIndexBufferHandle);

    glBindBuffer(GL_ARRAY_BUFFER, vertexDataBufferHandle);
    glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(VertexData), data, GL_STATIC_DRAW);

    glEnable(GL_ELEMENT_ARRAY_BUFFER);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vertexIndexBufferHandle);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6 * sizeof(unsigned int), indexes, GL_STATIC_DRAW);

    int loc;
    if ((loc = glGetAttribLocation(programHandle, posName)) != -1) {
        glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (GLuint *) 0);
        glEnableVertexAttribArray(loc);
    }
    if ((loc = glGetAttribLocation(programHandle, norName)) != -1) {
        glVertexAttribPointer(loc, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (GLuint *) (0 + sizeof(float) * 3));
        glEnableVertexAttribArray(loc);
    }
    if ((loc = glGetAttribLocation(programHandle, texName)) != -1) {
        glVertexAttribPointer(loc, 2, GL_FLOAT, GL_FALSE, sizeof(VertexData), (GLuint *) (0 + sizeof(float) * 6));
        glEnableVertexAttribArray(loc);
    }
    glBindVertexArray(0);
}

void MyObject::draw()
{
    glBindVertexArray(vertexArrayHandle);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);
}
