//standard libraries
#include <iostream>
#include <time.h>
#include <cstring>
#include <cstdio>

//opengl headers
#include <GL/glew.h>
#include <GL/freeglut.h>

//opengl mathematics
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/quaternion.hpp>

//text rendering library
//#include <ft2build.h>
//#include FT_FREETYPE_H

//functions for shader compilation and linking
#include "shaderhelper.h"
//landscape
#include "Landscape.h"
#include "MyObject.h"
#include "VertexData.h"

GLenum err;
char buf[1024];

#define DEBUG() \
    err = glGetError(); \
    if (err != GL_NO_ERROR) { \
        switch (err) { \
            case GL_INVALID_ENUM: \
                printf("GL_INVALID_ENUM %d\n", __LINE__ - 1); \
                break; \
            case GL_INVALID_VALUE: \
                printf("GL_INVALID_VALUE %d\n", __LINE__ - 1); \
                break; \
            case GL_INVALID_OPERATION: \
                printf("GL_INVALID_OPERATION %d\n", __LINE__ - 1); \
                break; \
            case GL_INVALID_FRAMEBUFFER_OPERATION: \
                printf("GL_INVALID_FRAMEBUFFER_OPERATION %d\n", __LINE__ - 1); \
                break; \
            case GL_OUT_OF_MEMORY: \
                printf("GL_OUT_OF_MEMORY %d\n", __LINE__ - 1); \
                break; \
            case GL_STACK_UNDERFLOW: \
                printf("GL_STACK_UNDERFLOW %d\n", __LINE__ - 1); \
                break; \
            case GL_STACK_OVERFLOW: \
                printf("GL_STACK_OVERFLOW %d\n", __LINE__ - 1); \
                break; \
            default: \
                printf("Shit! %d\n", __LINE__ - 1); \
                break; \
        } \
    }

using namespace std;
using glm::cross;
using glm::rotate;
using glm::normalize;
using glm::vec3;
using glm::sqrt;

//freetype staff
//FT_Library ft;
//FT_Face face;
//int ftInit;
//int ftError;

//ladscape parameters
unsigned int sizeLog = 8;
float edgeCoef = 0.2f;
float scale = 1.0f;

//timer staff
int startTime, endTime;
int prevTime;
float deltaTime;
int dtc = 0;
float deltaTimes[100];

Landscape *landscape;
MyObject *obj;

//struct for loading shaders
ShaderProgram shaderProgram;

//window size
int windowWidth = 800;
int windowHeight = 600;

//last mouse coordinates
int mouseX = windowWidth / 2;
int mouseY = windowHeight / 2;
int shiftX, shiftY;

//camera position
glm::vec3 eye;
glm::vec3 eyeStart;
//camera direction
glm::vec3 dir;
glm::vec3 dirStart;
//reference point position
glm::vec3 cen;
glm::vec3 cenStart;
//up vector direction (head of observer)
glm::vec3 up(0, 1, 0);
//used for camera movement
glm::vec3 shift;
glm::vec3 v05(.25, .25, .25);
glm::vec3 v25(1, 1, 1);

// sun position
glm::vec3 sunPosition(1 << (sizeLog - 1), 1000, 1 << (sizeLog - 1));

//matrices
glm::mat4x4 modelViewMatrix;
glm::mat4x4 projectionMatrix;
glm::mat4x4 modelViewProjectionMatrix;
glm::mat4x4 normMatrix;

///defines drawing mode
bool useTexture = true;

//texture identificator
GLuint texId;

//names of shader files. program will search for them during execution
//don't forget place it near executable
char VertexShaderName[] = "Vertex.vert";
char FragmentShaderName[] = "Fragment.frag";

////////////////////////////////////////////////////////////////////////
///this function prints miscellaneous debug information
void printDebugInfo()
{
    printf("position:( %3f, %3f, %3f )\n", eye.x, eye.y, eye.z);
    printf("direction:( %3f, %3f, %3f )\n", dir.x, dir.y, dir.z);
    printf("shiftX = %d, shiftY = %d\n", shiftX, shiftY);
    printf("windowSize:( %d, %d )", windowWidth, windowHeight);
    printf("    ");
    //printf("freetype initialized: %s\n", ftInit ? "no" : "yes");
    //printf("freetype error: %d\n", ftError);
}

/////////////////////////////////////////////////////////////////////
///is called when program starts
void init()
{
    //enable depth test
    glEnable(GL_DEPTH_TEST);
	glEnable(GL_LINE_SMOOTH);
	glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
    //initialize shader program
    shaderProgram.init(VertexShaderName, FragmentShaderName);
    //use this shader program
    glUseProgram(shaderProgram.programObject);

    obj = new MyObject();
    landscape = new Landscape(sizeLog, edgeCoef, scale);
    landscape->generateData();
    //initialize opengl buffers and variables inside of object
    landscape->initGLBuffers(shaderProgram.programObject, "pos", "nor", "tex");
    obj->initGLBuffers(shaderProgram.programObject, "posO", "norO", "texO");
    eye = eyeStart = vec3((1 << (sizeLog - 1)) * scale, landscape->getYMax(), 0);
    dir = dirStart = vec3(0, 0, 1);
    //cen = cenStart = vec3((1 << (sizeLog - 1)) * scale, landscape->getYMax(), 1);
    //ftInit = FT_Init_FreeType(&ft);
    //ftError = FT_New_Face(ft, "/usr/share/fonts/truetype/ubuntu-font-family/Ubuntu-M.ttf", 0, &face);
}

/////////////////////////////////////////////////////////////////////
///called when window size is changed
void reshape(int width, int height)
{
    windowWidth = width;
    windowHeight = height;
    //set viewport to match window size
    glViewport(0, 0, width, height);

    float fieldOfView = 45.0f;
    float aspectRatio = float(width)/float(height);
    float zNear = 0.1f;
    float zFar = 500.0f;
    //set projection matrix
    projectionMatrix = glm::perspective(fieldOfView, aspectRatio, zNear, zFar);
}

////////////////////////////////////////////////////////////////////
///actions for single frame
void display()
{
    printDebugInfo();
    startTime = clock();
    //prevTime = glutGet(GLUT_ELAPSED_TIME);

    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    //Draw triangle with shaders (in screen coordinates)
    //need to set uniform in modelViewMatrix

    //glUseProgram(shaderProgram.programObject);

    //camera matrix. camera is placed in point "eye" and looks at point "cen".
    //glm::mat4x4 viewMatrix = glm::lookAt(eye, cen, up);
    glm::mat4x4 viewMatrix = glm::lookAt(eye, eye + dir, up);

    //modelMatrix is connected with current object. move square to scene's center
    glm::mat4x4 modelMatrix = glm::translate(glm::mat4(), glm::vec3(0, 0, 0));

    //modelViewMatrix consists of viewMatrix and modelMatrix
    modelViewMatrix = viewMatrix*modelMatrix;

    //calculate normal matrix
    normMatrix = glm::inverseTranspose(modelViewMatrix);

    //finally calculate modelViewProjectionMatrix
    modelViewProjectionMatrix = projectionMatrix*modelViewMatrix;

    // Printing text on screen hopefully
    //glColor3f(1.0f, 1.0f, 1.0f);
    //glRasterPos2f(100, 100);
    glPushMatrix();
    glTranslatef(0.0f, 0.0f, 0.0f);
    char *text = "Sup?";
    size_t len = strlen(text);
    for (char *p = text; p - text < len; p++) {
        glutStrokeCharacter(GLUT_STROKE_MONO_ROMAN, *p);
    }
    glPopMatrix();

    //pass variables to the shaders
    int locMV = glGetUniformLocation(shaderProgram.programObject, "modelViewMatrix");
    if (locMV > -1)
        glUniformMatrix4fv(locMV, 1, 0, glm::value_ptr(modelViewMatrix));

    int locN = glGetUniformLocation(shaderProgram.programObject, "normMatrix");
    if (locN > -1)
        glUniformMatrix4fv(locN, 1, 0, glm::value_ptr(normMatrix));

    int locP = glGetUniformLocation(shaderProgram.programObject, "modelViewProjectionMatrix");
    if (locP > -1)
        glUniformMatrix4fv(locP, 1, 0, glm::value_ptr(modelViewProjectionMatrix));

    int locMin = glGetUniformLocation(shaderProgram.programObject, "yMin");
    if (locMin > -1)
        glUniform1f(locMin, landscape->getYMin());

    int locMax = glGetUniformLocation(shaderProgram.programObject, "yMax");
    if (locMax > -1)
        glUniform1f(locMax, landscape->getYMax());

    int locSun = glGetUniformLocation(shaderProgram.programObject, "sunPosition");
    if (locSun > -1)
        glUniform3fv(locSun, 1, glm::value_ptr(sunPosition));

    int locV = glGetUniformLocation(shaderProgram.programObject, "viewMatrix");
    if (locV > -1)
        glUniformMatrix4fv(locV, 1, 0, glm::value_ptr(viewMatrix));

    //if there is some problem
    if (locMV < 0 || locN < 0 || locP < 0 || locMin < 0 || locMax < 0 || locV < 0) {
        cout << "locMV = " << (locMV < 0 ? 0 : 1) << ", ";
        cout << "locN = " << (locN < 0 ? 0 : 1) << ", ";
        cout << "locP = " << (locP < 0 ? 0 : 1) << ", ";
        cout << "locMin = " << (locMin < 0 ? 0 : 1) << ", ";
        cout << "locMax = " << (locMax < 0 ? 0 : 1) << ", ";
        cout << "locV = " << (locV < 0 ? 0 : 1) << endl;
        //not all uniforms were allocated - show blue screen.
        //check your variables properly. May be there is unused?
        glClearColor(0.7, 0.4, 0.1, 1);
        glClear(GL_COLOR_BUFFER_BIT);
    } else { //draw square
        landscape->draw();
        obj->generateData(eye, dir, up);
        obj->draw();
    }
    //end frame visualization
    glutSwapBuffers();
    endTime = clock();
    //deltaTimes[dtc++] = float(endTime - startTime) / CLOCKS_PER_SEC;
}

//////////////////////////////////////////////////////////////////////////
///IdleFunction
void update()
{
    //make animation
    glutPostRedisplay();
}

void special(int key, int x, int y)
{
    //deltaTime = glutGet(GLUT_ELAPSED_TIME) - prevTime;
    if (key == 0x65) { // UP
        shift = v25 * normalize(dir);
        eye += shift;
        //cen += shift;
    } if (key == 0x67) { // DOWN
        shift = v25 * normalize(dir);
        eye -= shift;
        //cen -= shift;
    } if (key == 0x64) { // LEFT
        shift = v25 * normalize(cross(up, dir));
        eye += shift;
        //cen += shift;
    } if (key == 0x66) { // RIGHT
        shift = v25 * normalize(cross(up, dir));
        eye -= shift;
        //cen -= shift;
    } if (key == 0x68) { // PGUP
        shift = v25 * normalize(up);
        eye += shift;
        //cen += shift;
    } if (key == 0x69) { // PGDOWN
        shift = v25 * normalize(up);
        eye -= shift;
        //cen -= shift;
    }
}

/////////////////////////////////////////////////////////////////////////
///is called when key on keyboard is pressed
///use SPACE to switch mode
///TODO: place camera transitions in this function
void keyboard(unsigned char key, int mx, int my)
{
    //deltaTime = glutGet(GLUT_ELAPSED_TIME) - prevTime;
    if (key == 0x1b) { // <ESC>
        eye = eyeStart;
        //cen = cenStart;
        dir = dirStart;
        up = vec3(0, 1, 0);
    } if (key == 'w') {
        shift = v05 * normalize(dir);
        eye += shift;
        //cen += shift;
    } if (key == 'W') {
        shift = v25 * normalize(dir);
        eye += shift;
        //cen += shift;
    } if (key == 's') {
        shift = v05 * normalize(dir);
        eye -= shift;
        //cen -= shift;
    } if (key == 'S') {
        shift = v25 * normalize(dir);
        eye -= shift;
        //cen -= shift;
    } if (key == 'a') {
        shift = v05 * normalize(cross(up, dir));
        eye += shift;
        //cen += shift;
    } if (key == 'A') {
        shift = v25 * normalize(cross(up, dir));
        eye += shift;
        //cen += shift;
    } if (key == 'd') {
        shift = v05 * normalize(cross(up, dir));
        eye -= shift;
        //cen -= shift;
    } if (key == 'D') {
        shift = v25 * normalize(cross(up, dir));
        eye -= shift;
        //cen -= shift;
    } if (key == 'e') {
        shift = v05 * normalize(up);
        eye += shift;
        //cen += shift;
    } if (key == 'E') {
        shift = v25 * normalize(up);
        eye += shift;
        //cen += shift;
    } if (key == 'c') {
        shift = v05 * normalize(up);
        eye -= shift;
        //cen -= shift;
    } if (key == 'C') {
        shift = v25 * normalize(up);
        eye -= shift;
        //cen -= shift;
    }
}

/////////////////////////////////////////////////////////////////////////
///is called when mouse button is pressed
///TODO: place camera rotations in this function
void mouse(int button, int mode, int posx, int posy)
{
    //if (button == GLUT_LEFT_BUTTON && mode == GLUT_DOWN) {
        //shiftX = 0;
        //shiftY = 0;
        //mouseX = posx;
        //mouseY = posy;
    //}
}

/////////////////////////////////////////////////////////////
///(hopefully) is called when mouse is moved
///TODO: place camera rotations in this function
void mouseMove(int x, int y)
{
    //shiftX = mouseX - x;
    //shiftY = mouseY - y;
    shiftX = windowWidth / 2 - x;
    shiftY = windowHeight / 2 - y;
    //mouseX = x;
    //mouseY = y;
    if (shiftX || shiftY) {
        //up = rotate(up, float(shiftX) / 5, up);
        //cen = eye + rotate(dir, float(shiftX) / 5, up);
        dir = rotate(dir, float(shiftX) / 5, up);
        //up = rotate(up, -float(shiftY) / 5, cross(up, dir));
        //glm::vec3 direction = rotate(dir, -float(shiftY) / 5, cross(up, dir));
        //if (abs(direction.z) < 0.0001)
            //direction.z = 0.0001;
        //cen = direction;
        //dir = dir;
        //if (glm::sqrt(dir.x
        //cen = eye + rotate(dir, -float(shiftY) / 5, cross(up, dir));
        vec3 newDir = rotate(dir, -float(shiftY) / 5, cross(up, dir));
        if (newDir.x * dir.x > 0 && newDir.z * dir.z > 0)
            dir = newDir;

        glutWarpPointer(windowWidth / 2, windowHeight / 2);
    }
}

void mouseStay(int x, int y)
{
    shiftX = 0;
    shiftY = 0;
    mouseX = x;
    mouseY = y;
}
////////////////////////////////////////////////////////////////////////
///this function is used in case of InitializationError
void emptydisplay()
{
}

////////////////////////////////////////////////////////////////////////
///entry point
int main (int argc, char* argv[])
{
    glutInit(&argc, argv);
    glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE );
    glutInitContextVersion (3, 2);
    glutInitContextProfile(GLUT_CORE_PROFILE);
    glutInitContextFlags (GLUT_FORWARD_COMPATIBLE);
    glewExperimental = GL_TRUE;
    glutInitWindowPosition(10, 20);
    glutInitWindowSize(1000, 200);
    glutCreateWindow("Test OpenGL application");
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutReshapeWindow(windowWidth, windowHeight);
    glutIdleFunc(update);
    glutKeyboardFunc(keyboard);
    glutSpecialFunc(special);
    glutMouseFunc(mouse);
    glutPassiveMotionFunc(mouseMove);
    glutMotionFunc(mouseStay);
    glutSetCursor(GLUT_CURSOR_NONE);

    glewInit();
    cout << "OpenGL Version: " << glGetString(GL_VERSION) << endl;
    const char * slVer = (const char *) glGetString ( GL_SHADING_LANGUAGE_VERSION );
    cout << "GLSL Version: " << slVer << endl;

    try {
        init();
    }
    catch (const char *str) {
        cout << "Error During Initialiation: " << str << endl;
        //delete square;
        //delete sphere;
        delete obj;
        delete landscape;
        glDeleteTextures(1, &texId);
        //start main loop with empty screen
        glutDisplayFunc(emptydisplay);
        glutMainLoop();
        return -1;
    }

    try {
        glutMainLoop();
    }
    catch (const char *str) {
        cout << "Error During Main Loop: " << str << endl;
    }
    delete obj;
    delete landscape;

    return 0;
}
