#pragma once

struct VertexData
{
    glm::vec3 pos;
    glm::vec3 nor;
    glm::vec2 tex;
};
