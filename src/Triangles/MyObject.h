#pragma once

#include <glm/glm.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include "VertexData.h"

using namespace std;

class MyObject
{
	VertexData *data;
	unsigned int *indexes;
	unsigned int indexTop;

	unsigned int vertexArrayHandle;
	unsigned int vertexDataBufferHandle;
	unsigned int vertexIndexBufferHandle;

    //glm::vec3 eye;
    //glm::vec3 dir;
    //glm::vec3 up;
    glm::vec3 cen;
    glm::vec3 left;
    glm::vec3 top;

public:
	~MyObject();

	void generateData(glm::vec3 e, glm::vec3 d, glm::vec3 u);
	void initGLBuffers(GLuint programHandle, const char* posName, const char* norName, const char* texName);
	void draw();
};
