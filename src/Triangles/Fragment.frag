//shader version
#version 150 core

uniform mat4 viewMatrix;
//mode of drawing
//if is true, then use Texture
//otherwise draw gradient
//uniform int useTexture;

//texture object
//uniform sampler2D texture;

uniform vec3 sunPosition;
//vec3 direction_to_light_world  = vec3 (1.0, 1.0, 1.0);

// fixed point light properties
vec3 Ls = vec3 (1.0, 1.0, 1.0); // white specular colour
vec3 Ld = vec3 (0.7, 0.7, 0.7); // dull white diffuse light colour
vec3 La = vec3 (0.1, 0.1, 0.1); // grey ambient colour

// surface reflectance
vec3 Ks = vec3 (0.1, 0.1, 0.1);
vec3 Ka = vec3 (0.5, 0.5, 0.5);
float specular_exponent = 5.0; // specular 'power'

//retrieve this data form vertex shader
in VertexData
{
	vec3 position;
	vec3 normal;
	vec2 texcoord;
	vec4 color;
} VertexIn;

out vec4 gl_FragColor;

void main()
{
    vec3 tmpV;
    tmpV = (viewMatrix * vec4(sunPosition, 1.0)).xyz - VertexIn.position;
    vec3 direction_to_light_eye = normalize(tmpV);
    vec3 Id; // final diffuse intensity
    float tmpF;
    tmpF = max(dot(direction_to_light_eye, VertexIn.normal), 0.0f);
    if (VertexIn.color.b > 0.7)
        Id = vec3(0.4, 0.4, 0.4) * VertexIn.color.rgb * tmpF;
    else
        Id = Ld * VertexIn.color.rgb * tmpF;

    // specular intensity
    vec3 Is;
    tmpV = reflect(-direction_to_light_eye, VertexIn.normal);
    tmpF = max(dot(tmpV, normalize(-VertexIn.position)), 0.0f);
    if (VertexIn.color.b > 0.7)
        Is = Ls * vec3(1,1,1) * pow(tmpF, specular_exponent);
    else
        Is = Ls * Ks * pow(tmpF, specular_exponent);

    /*vec3 color = Is + Id + La * Ka;*/
    /*if (VertexIn.position.x > 1.0f)*/
        /*color.r = 1.0f;*/
    /*if (VertexIn.position.y > 1.0f)*/
        /*color.g = 1.0f;*/
    /*if (VertexIn.position.z < -10.0f)*/
        /*color.b = 1.0f;*/
    // final colour
    gl_FragColor = vec4 (Is + Id + La * Ka, 1.0);
    /*gl_FragColor = vec4(color, 1.0);*/
}
