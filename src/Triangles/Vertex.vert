//shader version
#version 150 core

uniform mat4 modelViewMatrix;

//projectionMatrix*modelViewMatrix
uniform mat4 modelViewProjectionMatrix;

//inverse and transpose matrix for normals
uniform mat4 normMatrix;

//parameters for color generation
uniform float yMin;
uniform float yMax;

//input vertex: position, normal, texture coordinates
in vec3 pos;
in vec3 nor;
in vec2 tex;
in vec3 posO;
in vec3 norO;
in vec2 texO;

//output vertex to fragment shader
out VertexData
{
	vec3 position;
	vec3 normal;
	vec2 texcoord;
	vec4 color;
} VertexOut;

void main()
{
	float y = pos.y;
	vec3 jopa = pos;
	vec3 jopec = nor;

	if (y > yMax - (yMax - yMin) * 0.2) {
		VertexOut.color = vec4(1.0, 1.0, 1.0, 1.0);
	} else if (y > yMin + (yMax - yMin) * 0.2 && y < yMin + (yMax - yMin) * 0.5) {
		VertexOut.color = vec4(0.1, 0.7, 0.3, 1.0);
	} else if (y <= yMin + (yMax - yMin) * 0.2) {
		VertexOut.color = vec4(0.1, 0.5, 0.8, 1.0);
		jopa.y = yMin + (yMax - yMin) * 0.2;
		jopec.xyz = vec3(0, 1, 0);
	} else {
		VertexOut.color = vec4(0.4, 0.2, 0.07, 1.0);
	}
//	VertexOut.color = vec4(lims.x, lims.y, 0.2f, 1.0f);
//	if (length(nor) > 1)
//		VertexOut.color = vec4(pow(1 / length(nor), 1/10), 0, 0, 1);
//	if (length(nor) < 1)
//		VertexOut.color = vec4(0, length(nor), 0, 1);
//	if (length(nor) == 1)
//		VertexOut.color = vec4(0, 0, 1, 1);

	gl_Position = modelViewProjectionMatrix*vec4(jopa.xyz,1);
	//gl_Position = vec4(pos.x,y,pos.y,1);

	VertexOut.position = vec3(modelViewMatrix*vec4(jopa.xyz,1));
	VertexOut.normal = vec3(normMatrix*vec4(jopec.xyz,1));	
	//VertexOut.normal = vec3(modelViewMatrix * vec4(nor.x,yn,nor.y,0));	
	VertexOut.texcoord = vec2(tex.xy);
}
