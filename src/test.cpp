#include <cstdio>
#include <cstdlib>
#include <ft2build.h>
#include FT_FREETYPE_H

int main(int argc, char* argv[])
{
    int num_chars = 1;

    if (argc > 1) {
        num_chars = atoi(argv[1]);
    }

    FT_Library ft;
    FT_Face face;
    char *facePath = "/usr/share/fonts/truetype/ubuntu-font-family/Ubuntu-M.ttf";

    if (FT_Init_FreeType(&ft)) {
        printf("FT_Init_FreeType failed");
    }
    if (FT_New_Face(ft, facePath, 0, &face)) {
        printf("FT_New_Face failed");
    }
    if (FT_Set_Char_Size(face, 0, 16*64, 300, 300)) {
        printf("FT_Set_Char_Size failed");
    }
    if (FT_Set_Pixel_Sizes(face, 0, 16)) {
        printf("FT_Set_Pixel_Sizes failed");
    }

    FT_GlyphSlot slot = face->glyph;  /* a small shortcut */
    int n;
    int pen_x = 300;
    int pen_y = 200;
    char text[num_chars];

    for (int i = 0; i < num_chars; i++) {
        text[i] = 'a' + i;
    }

    for (int n = 0; n < num_chars; n++) {
        /* load glyph image into the slot (erase previous one) */
        if (FT_Load_Char(face, text[n], FT_LOAD_RENDER)) {
            printf("FT_Load_Char failed");
        }

        /* now, draw to our target surface */
        //my_draw_bitmap(&slot->bitmap,
                //pen_x + slot->bitmap_left,
                //pen_y - slot->bitmap_top);
        printf("pen_x + slot->bitmap_left = %d\n", pen_x + slot->bitmap_left);
        printf("pen_y - slot->bitmap_top = %d\n", pen_y - slot->bitmap_top);
        printf("pen_x = %d; pen_y = %d\n", pen_x, pen_y);
        printf("slot->bitmap_left = %d; slot->bitmap_top = %d\n", slot->bitmap_left, slot->bitmap_top);
        int rows = slot->bitmap.rows;
        int width = slot->bitmap.width;
        unsigned char *buffer = slot->bitmap.buffer;
        putchar('\n');
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < width; j++) {
                if (buffer[i * width + j] > 0x87) {
                    //printf("%02x ", buffer[i * width + j]);
                    printf("00 ");
                } else {
                    printf("   ");
                }
            }
            putchar('\n');
        }
        putchar('\n');

        /* increment pen position */
        pen_x += slot->advance.x >> 6;
        pen_y += slot->advance.y >> 6; /* not useful for now */
    }

    return 0;
}
